from kotti.interfaces import IDocument
from kotti.resources import Content
from kotti.util import Link, LinkParent
import sqlalchemy
from zope.interface import implements

from kotti_glossary import _


class Glossary(Content):

    implements(IDocument)

    id = sqlalchemy.Column(
        sqlalchemy.Integer(), sqlalchemy.ForeignKey('contents.id'),
        primary_key=True)

    title = sqlalchemy.Column(sqlalchemy.String(255), nullable=False)

    type_info = Content.type_info.copy(
        name=u'Glossary',
        title=_(u'Glossary'),
        add_view=u'add_glossary',
        addable_to=[u'Document'],
    )

    def get_terms(self, request):
        """Retrurns a adict of term: url.
        """
        res = dict()
        for term in self.children:
            title = term.title.lower()
            anchor = 'term-%s' % title
            url = request.resource_url(self, anchor=anchor)
            res[title] = url
        return res


class Term(Content):

    id = sqlalchemy.Column(
        sqlalchemy.Integer(), sqlalchemy.ForeignKey('contents.id'),
        primary_key=True)

    title = sqlalchemy.Column(sqlalchemy.String(255), nullable=False)
    definition = sqlalchemy.Column(sqlalchemy.Text)

    type_info = Content.type_info.copy(
        name=u'Term',
        title=_(u'Term'),
        add_view=u'add_term',
        addable_to=[u'Glossary'],
    )


def hasbody(context, request):
    return hasattr(context, 'body')


Content.type_info.edit_links.append(
    LinkParent(
        title=_(u'Glossary actions'),
        children=[Link('scan-terms',
                       title=_(u'Scan terms'),
                       predicate=hasbody),
                  Link('reset-terms',
                       title=_(u'Reset terms'),
                       predicate=hasbody)],
    )
)
